## MODERATION, MICROSDOSING, TEMPERANCE

Maybe it's misnamed ... *meh, sobeit* ... CoffeeRxiv ***is*** about coffee, the beverage we love and more ... but it's MOSTLY about moderation. 

CoffeeRxiv also about not overdosing on the love ... that extends to other things we might want to try, but which we may not particularly love as much, like alcohol, cannabis, psychedelics, supplements, nutritional biochemistry, probiotics to manipulate the intestinal gut microflora, herbal medicines and, above all, detoxification. 

CoffeeRxiv is about **being more informed** or ready and able to actively participate in the peer-review process to critically appraise information. Critically-appraised information helps us MODERATE the swallowing of things, whereas propaganda from news organizations leads to unappraised swallowing of things.

Thus, CoffeeRxiv is MOSTLY a pre-print archive on the TEMPERATE enjoyment or microdosing of the so-called *natural* drugs like caffiene IN MODERATION.

CoffeeRxiv is for grown-ups of all ages ... definitely not for everyone; definitely not for swallowers. Of course, we definitely know that in some cases, there are some people who MUST abstain -- should never consume even one drop of a drug like caffeine. 


Temperance is about QUALITY, TASTE, OPTIMIZATION ... for some, the optimal amount will be zero or strict abstinence. In general population, however, it is best when people have generally INFORMED views of their consumption habits ... we believe that there are societal benefits to MODERATE, TEMPERATE, MICRO-DOSED consumption of things that can produce short bursts of extreme focus, extreme creativity, extreme productivity.

Everyone is a more or less an expert on their own tastebuds ... people tend to know how they like their coffee, so if they are going to consume something, perhaps they really should try to become something of an expert on their own choices of the [latest coffee publications](https://www.biorxiv.org/search/coffee).

MODERATION is also about Science, the scientific method and carefully, critically examining RATHER THAN JUST SWALLOWING. In other words, let's see if we can do something in this specific little coffee klatch about democratizing the peer-review process without someone claiming that someone is misleading someone. Moderation.

**Obviously, do not believe anything you read here ... OR ANYWHERE ELSE!** *MODERATION means that you should STOP just swallowing things when you don't know where those things have bean.*