---
title: Metabolomics
subtitle: You Must Always Get What You NEED
date: 2022-03-10
tags: ["TBD0", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

What do we need coffee for?

Maybe we don't exactly NEED coffee. 

Maybe we roast beans and brew coffee in order to metabolize and experience different kinds of tastes and sensations ... because maybe we NEED the experience of making and drinking excellent coffee. 

[Metabolomics Combined with Sensory Analysis Reveals the Impact of Different Extraction Methods on Coffee Beverages from Coffea arabica and Coffea canephora var. Robusta](https://www.mdpi.com/1538532)