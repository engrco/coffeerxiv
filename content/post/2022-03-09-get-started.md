---
title: Genuinely Debilitating Pain, Fibromyalgia, Complex Regional Pain Syndrome
subtitle: My Dog Died Yesterday, Left Me All Alone
date: 2022-03-09
tags: ["TBD0", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Debilitating pain is no joke ... which is why we have to joke about it. It takes an enormous amount of patience to deal with pain, while developing a strategy to do something about that pain. Almost no one has that kind of patience ... so we try humor. 

Mostly, it's just impossible though ...

Understanding mineral balances / unbalances is probably something that could effect every human ...  it's not just getting some [but not too much] Sun and getting your vitamin D levels up [or vitamin K, C, etc], we might also need things like serious cold exposure to develop brown adipose tissue and some [but not too much] *natural dirt* and minerals in our food to make our intestinal microflora happy ... our affluent environments are dreadfully deficient in many ways that we don't yet understand ... which is dangerously deep in *knack for obvious* territory, but it's not something your *medical professional* is probably going to tell you.

Relatively quickly ... perhaps not quickly enough ... people eventually learn that they cannot just mask pain with opioids, cannabanoids, pschydelics ... in many of these kinds of pain things, it's not what you add ... but instead what you take away ... UNLESS we are talking about some sort of deficiency ... many people, maybe most, for example are deficient in things like magnesium or iodine and have swallowed way too much alcohol or smoke way too much dope, so that they don't have a prayer of getting out of the haze to sort out the ROOT CAUSE of their pain. 

This stuff really requires a deep, deep, deep understanding of things like intestinal ecosystems, biochemistry and taking super deep dives into topics like nutrition and exercise ... but you need to [start somewhere and develop a skill in exploring connections](https://www.connectedpapers.com/main/5eea003920ce360a63bb94912d6b1f507849d942/Bisphosphonates-in-the-treatment-of-complex-regional-pain-syndrome%3A-is-bone-the-main-player-at-early-stage-of-the-disease%3F/graph)