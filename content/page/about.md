---
title: CoffeeRxiv and Moderation
subtitle: Seeking NEW extremes, expand the pi
comments: false
---


Put down your publications and books ... join in the process of DRIVING the expansion of knowledge ... *expand the pi...* 

The strenuous promotion of *absolute* abstention of things, like caffiene, that should be consumed in moderation is for religious fundamentalists and [anti-caffeine ayatollahs](https://michaelpollan.com/books/caffeine-how-coffee-and-tea-created-the-modern-world/), even as they claim to understand why caffiene ***WAS*** necessary *in the past, before they were* ***enlightened***. 

Screw enlightenment! ***We ain't there yet*** ... fortunately, there are [still things to do.](https://engrco.gitlab.io/coffeerxiv/page/process/)

Maybe it takes a heretic or infidel to encourage people to occasionally use things that lead to extreme focus, extreme creativity, extreme ideas ... but the ABSOLUTE truth is that we cannot be either FREE or build the stock of knowledge which makes us even more free without MODERATE exploration of extremes.

*Yeah, we know that you can do this better ... [just go fork yourself](https://gitlab.com/engrco/coffeerxiv)*