---
title: Participating In Peer-Review
subtitle: Join in the PROCESS of unsettling the Science
comments: false
---

Screw enlightenment! ***We ain't close to being there yet!***

The PROCESS is the fun part, the destination is something akin to death, ie not much to see there. *Yes, princess ... everything dies.* 

CoffeeRxiv is a project of the GitRxiv group ... this whole game all about the application of deep learning and artificial intelligence to examine Git repositories of ideas that are conversations of bleeding-edge pre-print archives.

We believe that Science ***IS*** the peer-review process **itself** ...

... which means that Science is NOT really about those dead artifacts of past peer-review processes, although we stand on the shoulders of dead, imperfect and incomplete published artifacts of the scientific process.

The PRINTED Science is something for the rear-view mirror ... yes, of course, very important, but not really where we are GOING ... if we want to DRIVE knowledge, we must actively, critically participate in the peer-review process ... in a *no holds barred* fashion.

*Yeah, we know that you can do this better ... [just go fork yourself](https://gitlab.com/engrco/coffeerxiv)*